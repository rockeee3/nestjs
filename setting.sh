#!/bin/bash

#Nestjs 테스트 세팅

#Mysql docker
docker pull --platform linux/amd64 mysql:5
docker run --name mysql1 -e MYSQL_ROOT_PASSWORD=mysql1 -p 52001:3306  -d mysql:5 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
docker run --name mysql2 -e MYSQL_ROOT_PASSWORD=mysql2 -p 52002:3306  -d mysql:5 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

#Sequel ace
brew install —cask sequel-ace

