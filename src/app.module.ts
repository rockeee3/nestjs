import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { emailConfig } from './config/emailConfig';
import { db1Config, db2Config } from './config/dbConfig';
import { configValidationSchema } from './config/validation'
import { TypeOrmModule } from '@nestjs/typeorm';
// import emailConfig from './config/emailConfig';
// import { emailConfig, validationSchema } from './config/emailConfig';

@Module({
  imports: [

    // // 방법1 : configModule을 통해 파일로부터 환경변수를 읽은 후, 해당 환경변수를 활용

    // ConfigModule.forRoot({
    //   envFilePath: [`${__dirname}/config/env/.${process.env.NODE_ENV}.env`],
    //   load: [emailConfig],
    //   isGlobal: true,
    //   validationSchema: configValidationSchema,
    // }),
    // TypeOrmModule.forRoot({
    //   name: 'mysql1',
    //   type: 'mysql',
    //   host: process.env.DATABASE_1_HOST, //'localhost',
    //   port: +process.env.DATABASE_1_PORT, //52001,
    //   username: process.env.DATABASE_1_USERNAME, //'root',
    //   password: process.env.DATABASE_1_PASSWORD, //'mysql1',
    //   database: 'mysql1',
    //   entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //   synchronize: Boolean(process.env.DATABASE_1_SYNCHRONIZE), // true,
    //   extra: {}, // https://github.com/mysqljs/mysql#pool-options
    // }),
    // TypeOrmModule.forRoot({
    //   name: 'mysql2',
    //   type: 'mysql',
    //   host: process.env.DATABASE_2_HOST, //'localhost',
    //   port: +process.env.DATABASE_2_PORT, //52001,
    //   username: process.env.DATABASE_2_USERNAME, //'root',
    //   password: process.env.DATABASE_2_PASSWORD, //'mysql1',
    //   database: 'mysql2',
    //   entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //   synchronize: Boolean(process.env.DATABASE_2_SYNCHRONIZE), // true,
    // }),

    // // 방법2 : configModule을 통해 파일로부터 환경변수를 읽은 후, 해당 환경변수를 활용 (1과 비슷)

    // ConfigModule.forRoot({
    //   envFilePath: [`${__dirname}/config/env/.${process.env.NODE_ENV}.env`],
    //   load: [emailConfig],
    //   isGlobal: true,
    //   validationSchema: configValidationSchema,
    // }),
    // TypeOrmModule.forRootAsync({
    //   name: 'mysql1',
    //   // imports: [ConfigModule],
    //   useFactory: (configService: ConfigService) => ({
    //     type: 'mysql',
    //     host: configService.get('DATABASE_1_HOST'),
    //     port: +configService.get<number>('DATABASE_1_PORT'),
    //     username: configService.get('DATABASE_1_USERNAME'),
    //     password: configService.get('DATABASE_1_PASSWORD'),
    //     database: 'mysql1',
    //     entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //     synchronize: Boolean(configService.get('DATABASE_1_SYNCHRONIZE')),
    //   }),
    //   inject: [ConfigService],
    // }),
    // TypeOrmModule.forRootAsync({
    //   name: 'mysql2',
    //   // imports: [ConfigModule],
    //   useFactory: (configService: ConfigService) => ({
    //     type: 'mysql',
    //     host: configService.get('DATABASE_2_HOST'),
    //     port: +configService.get<number>('DATABASE_2_PORT'),
    //     username: configService.get('DATABASE_2_USERNAME'),
    //     password: configService.get('DATABASE_2_PASSWORD'),
    //     database: 'mysql2',
    //     entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //     synchronize: Boolean(configService.get('DATABASE_2_SYNCHRONIZE')),
    //   }),
    //   inject: [ConfigService],
    // }),

    // 방법3 : configModule을 통해 파일로부터 환경변수를 읽은 후, 해당 환경객체 사용

    ConfigModule.forRoot({
      envFilePath: [`${__dirname}/config/env/.${process.env.NODE_ENV}.env`],
      load: [emailConfig, db1Config, db2Config],
      isGlobal: true,
      validationSchema: configValidationSchema,
    }),
    TypeOrmModule.forRootAsync({
      name: 'mysql1',
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) =>
        await configService.get('db1'),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      name: 'mysql2',
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) =>
        await configService.get('db2'),
      inject: [ConfigService],
    }),
    UsersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
