import { registerAs } from "@nestjs/config";

// 와우!
// https://stackoverflow.com/questions/67166620/how-to-get-values-from-custom-configuration-file-in-nestjs-with-interface

export interface TypeOrmConfig {
    type: string;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    synchronize: boolean;
    logging: boolean;
    entities: any[];
}

export const db1Config = registerAs('db1', (): TypeOrmConfig => ({
    type: 'mysql',
    host: process.env.DATABASE_1_HOST,
    port: +process.env.DATABASE_1_PORT,
    username: process.env.DATABASE_1_USERNAME,
    password: process.env.DATABASE_1_PASSWORD,
    database: 'mysql1',
    logging: true,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: Boolean(process.env.DATABASE_1_SYNCHRONIZE),
}));

export const db2Config = registerAs('db2', (): TypeOrmConfig => ({
    type: 'mysql',
    host: process.env.DATABASE_2_HOST,
    port: +process.env.DATABASE_2_PORT,
    username: process.env.DATABASE_2_USERNAME,
    password: process.env.DATABASE_2_PASSWORD,
    database: 'mysql2',
    logging: true,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: Boolean(process.env.DATABASE_2_SYNCHRONIZE),
}));

// export const db1Config = registerAs('db1', () => ({
//     host: process.env.DATABASE_1_HOST,
//     port: process.env.DATABASE_1_PORT,
//     auth: {
//         user: process.env.DATABASE_1_USERNAME,
//         pass: process.env.DATABASE_1_PASSWORD,
//     },
//     sync: process.env.DATABASE_1_SYNCHRONIZE,
// }));

// export const db2Config = registerAs('db2', () => ({
//     host: process.env.DATABASE_2_HOST,
//     port: process.env.DATABASE_2_PORT,
//     auth: {
//         user: process.env.DATABASE_2_USERNAME,
//         pass: process.env.DATABASE_2_PASSWORD,
//     },
//     sync: process.env.DATABASE_2_SYNCHRONIZE,
// }));
