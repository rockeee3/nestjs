import { plainToClass } from 'class-transformer';
import { IsEnum, IsNumber, validateSync } from 'class-validator';
import * as Joi from 'joi';

export const configValidationSchema = Joi.object({
    EMAIL_SERVICE: Joi.string()
        .required(),
    EMAIL_AUTH_USER: Joi.string()
        .required(),
    EMAIL_AUTH_PASSWORD: Joi.string()
        .required(),
    EMAIL_BASE_URL: Joi.string()
        .required()
        .uri(),
    DATABASE_1_HOST: Joi.string()
        .required(),
    DATABASE_1_PORT: Joi.string()
        .required(),
    DATABASE_1_USERNAME: Joi.string()
        .required(),
    DATABASE_1_PASSWORD: Joi.string()
        .required(),
    DATABASE_1_SYNCHRONIZE: Joi.string()
        .required(),
    DATABASE_2_HOST: Joi.string()
        .required(),
    DATABASE_2_PORT: Joi.string()
        .required(),
    DATABASE_2_USERNAME: Joi.string()
        .required(),
    DATABASE_2_PASSWORD: Joi.string()
        .required(),
    DATABASE_2_SYNCHRONIZE: Joi.string()
        .required()
});
