import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, BadRequestException, Header, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UserLoginDto } from './dto/user-login.dto';
import { VerifyEmailDto } from './dto/verify-email.dto';
import { UserInfo } from './userInfo';
// import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async createUser(@Body() dto: CreateUserDto): Promise<void> {
    const { name, email, password } = dto;
    await this.usersService.createUser(name, email, password);
  }

  @Post('/email-verify')
  async verifyEmail(@Query() dto: VerifyEmailDto): Promise<string> {
    const { signupVerifyToken } = dto;
    return await this.usersService.verifyEmail(signupVerifyToken);
  }

  @Post('/login')
  async login(@Body() dto: UserLoginDto): Promise<string> {
    console.log(dto)
    const { email, password } = dto;
    return await this.usersService.login(email, password);
  }

  @Get('/:id')
  async getUserInfo(@Param('id') userId: string): Promise<UserInfo> {
    console.log(userId);
    return await this.usersService.getUserInfo(userId);
  }

  // @Post()
  // create(@Body() dto: CreateUserDto) {
  //   const { name, email, password } = dto;
  //   await this.usersService.createUser(name, email, password);
  //   // return this.usersService.create(createUserDto);
  // }

  // @Header('Custom', 'Test Header') // 커스텀 헤더 추가
  // @Get()
  // findAll() {
  //   return this.usersService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: number) { // 타입 변환 실패시에도 걍 처리됨. 별도 validation 필요
  //   if (+id < 1) {
  //     // exception을 통해 매핑된 http code를 리턴한다.
  //     // exception 내용은 return내 message 필드에 반영됨
  //     throw new BadRequestException('id는 0보다 큰 값이어야 합니다.');
  //   }
  //   return this.usersService.findOne(+id);
  // }

  // @HttpCode(202) // 예외 미발생 성공시 리턴값
  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
  //   return this.usersService.update(+id, updateUserDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.usersService.remove(+id);
  // }
}
